package org.erpk.parser;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;

public class CountryEconomyParserTest {

	String html;
	
	@Before
	public void setup() throws IOException{
		File file = new File("src-test/org/erpk/parser/economy.htm");
		html = FileUtils.readFileToString(file);
	}
	
	@Test
	public void testParse() {
	
		JSONObject stats = new CountryEconomyParser().parse(html);
		
		System.out.println(stats);
	}

}
