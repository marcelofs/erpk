package org.erpk.parser;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;

public class CountryMilitaryParserTest {

	String html;
	
	@Before
	public void setup() throws IOException{
		File file = new File("src-test/org/erpk/parser/military.htm");
		html = FileUtils.readFileToString(file);
	}
	
	@Test
	public void testParse() {
	
		JSONObject stats = new CountryMilitaryParser().parse(html);
		
		System.out.println(stats);
	}

}
