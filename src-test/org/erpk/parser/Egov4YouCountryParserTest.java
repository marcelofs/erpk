package org.erpk.parser;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;

public class Egov4YouCountryParserTest {
	public static void main(String[] args) throws IOException {
		File file = new File("src-test/org/erpk/parser/egovCountry.html");
		String html = FileUtils.readFileToString(file);
		JSONArray stats = new Egov4YouCountryParser().parse(html);
		System.out.println(stats.toString());
	}

}
