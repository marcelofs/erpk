package org.erpk.parser;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;

public class PartyMembersParserTest {
	public static void main(String[] args) throws IOException {
		File file = new File("src-test/org/erpk/parser/arena.html");
		String html = FileUtils.readFileToString(file);
		JSONArray partyMembers = new PartyMembersParser().parse(html);
		System.out.println(partyMembers.toString());
	}

}
