package org.erpk.parser;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;

public class PartyParserTest {

	String html;
	
	@Before
	public void setup() throws IOException{
		File file = new File("src-test/org/erpk/parser/arena-party.htm");
		html = FileUtils.readFileToString(file);
	}
	
	@Test
	public void testParse() {
	
		JSONObject stats = new PartyParser().parse(html);
		
		assertEquals("536", stats.get("members").toString());
		System.out.println(stats);
	}

}
