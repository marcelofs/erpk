package org.erpk.parser;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;

public class CountryLawsParserTest {
	public static void main(String[] args) throws IOException {
		File file = new File("src-test/org/erpk/parser/countryLaws.htm");
		String html = FileUtils.readFileToString(file);
		JSONArray stats = new CountryLawsParser("Brazil").parse(html);
		System.out.println(stats.toString());
	}

}
