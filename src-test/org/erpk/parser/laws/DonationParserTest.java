package org.erpk.parser.laws;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class DonationParserTest {
	public static void main(String[] args) throws IOException {
		File file = new File("src-test/org/erpk/parser/laws/donation.htm");
		String html = FileUtils.readFileToString(file);
		JSONObject stats = new DonationParser().parse(html);
		System.out.println(stats.toString());
	}

}
