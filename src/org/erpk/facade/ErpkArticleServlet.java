package org.erpk.facade;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.erpk.ErepScrapper;
import org.erpk.eRepTimeCalculator;
import org.erpk.eRepURL;
import org.erpk.parser.ArticleParser;
import org.json.simple.JSONObject;

public class ErpkArticleServlet extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	@SuppressWarnings("unchecked")
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		
		String id = req.getParameter("id");

		if (id == null || id.trim().isEmpty()) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}

		ErepScrapper scrapper = new ErepScrapper();

		try {

			String html = scrapper.scrape(eRepURL.ARTICLE.getUrl(id));
			JSONObject account = new ArticleParser().parse(html);

			String day = (String) account.remove("eRepDay");
			account.put("eRepDay", calculateDay(day));

			resp.getWriter().write(account.toString());

		} catch (NullPointerException e) {
			JSONObject obj = new JSONObject();
			obj.put("status", "deleted");
			resp.getWriter().write(obj.toString());
		} finally {
			scrapper.shutdown();
		}
	}

	private Integer calculateDay(String day) {

		Integer today = eRepTimeCalculator.geteRepDay();

		// possible bug if X hours ago leads to yesterday, but who cares?
		if (day.contains("hours ago") || day.contains("minutes ago"))
			return today;

		if (day.contains("days ago"))
			return today - Integer.valueOf(day.replace("days ago", "").trim());

		if (day.contains("Day"))
			return Integer.valueOf(day.substring(4, 9).replace(",", "").trim());

		return 0;
	}
}
