package org.erpk.facade;

import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.erpk.ErepScrapper;
import org.erpk.eRepURL;
import org.erpk.data.PMF;
import org.erpk.data.ProposedLaw;
import org.erpk.parser.CountryLawsParser;
import org.erpk.parser.laws.ILawParser;
import org.erpk.parser.laws.LawParserFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ErpkCountryLawsServlet extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	private static final Logger	logger				= Logger.getLogger(ErpkCountryLawsServlet.class
															.getName());

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		
		String id = req.getParameter("country");

		if (id == null || id.trim().isEmpty()) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}

		ErepScrapper scrapper = new ErepScrapper();

		try {
			scrapper.login();

			String html = scrapper.scrape(eRepURL.COUNTRY_LAWS.getUrl(id));
			JSONArray account = new CountryLawsParser(id).parse(html);

			PersistenceManager pm = PMF.getNewPersistenceManager();
			JSONParser jsParser = new JSONParser();
			LawParserFactory parserFactory = new LawParserFactory();

			Iterator it = account.iterator();
			while (it.hasNext()) {
				JSONObject jsObj = (JSONObject) it.next();
				String lawId = jsObj.get("id").toString();

				ProposedLaw law = null;
				try {
					law = pm.getObjectById(ProposedLaw.class, lawId);
				} catch (JDOObjectNotFoundException e) {

					logger.log(Level.WARNING, "Reaching for law id "
							+ jsObj.get("id").toString());

					ILawParser parser = parserFactory.getParser(jsObj.get(
							"type").toString());

					if (parser != null) {
						String lawHtml = scrapper.scrape(eRepURL.LAW
								.getUrl(jsObj.get("id").toString()));
						JSONObject lawDetails = parser.parse(lawHtml);

						law = new ProposedLaw();
						law.setId(jsObj.get("id").toString());
						law.setDetails(lawDetails.toString());
					}
					pm.makePersistent(law);
				}

				if (law != null && law.getDetails() != null)
					jsObj.put("details", jsParser.parse(law.getDetails()));

			}

			resp.getWriter().write(account.toString());

		} catch (ParseException e) {
			throw new IOException(e);
		} finally {
			scrapper.shutdown();
		}
	}

}
