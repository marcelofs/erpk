package org.erpk.facade;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.erpk.ErepScrapper;
import org.erpk.eRepURL;
import org.erpk.parser.CitizenParser;

public class ErpkCitizenServlet extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		
		String id = req.getParameter("id");

		if (id == null || id.trim().isEmpty()) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}

		ErepScrapper scrapper = new ErepScrapper();

		try {
			String html = scrapper.scrape(eRepURL.CITIZEN.getUrl(id));

			resp.getWriter().write(new CitizenParser().parse(html).toString());

		} finally {
			scrapper.shutdown();
		}
	}
}
