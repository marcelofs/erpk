package org.erpk.facade;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.erpk.ErepScrapper;
import org.erpk.eRepURL;
import org.erpk.parser.CountryEconomyParser;
import org.erpk.parser.CountrySocietyParser;
import org.json.simple.JSONObject;

public class ErpkCountrySocietyServlet extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	private static final Logger	logger				= Logger.getLogger(ErpkCountrySocietyServlet.class
															.getName());

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		
		String id = req.getParameter("country");

		if (id == null || id.trim().isEmpty()) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}

		ErepScrapper scrapper = new ErepScrapper();

		try {
//			scrapper.login();

			String html = scrapper.scrape(eRepURL.COUNTRY_SOCIETY.getUrl(id));
			JSONObject account = new CountrySocietyParser().parse(html);

			resp.getWriter().write(account.toString());

		} finally {
			scrapper.shutdown();
		}
	}

}
