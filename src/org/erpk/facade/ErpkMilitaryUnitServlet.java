package org.erpk.facade;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.erpk.ErepScrapper;
import org.erpk.eRepURL;
import org.erpk.parser.MilitaryUnitParser;
import org.json.simple.JSONObject;

public class ErpkMilitaryUnitServlet extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	private static final Logger	logger				= Logger.getLogger(ErpkMilitaryUnitServlet.class
															.getName());

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");

		String id = req.getParameter("id");
		String regiment = req.getParameter("regiment");

		if (id == null || id.trim().isEmpty()) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}

		ErepScrapper scrapper = new ErepScrapper();

		try {
			scrapper.login();

			if (regiment == null || regiment.trim().isEmpty()) {
				String html = scrapper.scrape(eRepURL.MU_REGIMENTS.getUrl(id));
				JSONObject mu = new MilitaryUnitParser().parseGroups(html);
				resp.getWriter().write(mu.toString());
			} else {
				
				Header header = new BasicHeader("X-Requested-With", "XMLHttpRequest");
			
				String html = scrapper.scrape(eRepURL.MU_REGIMENTS.getUrl(id)
						+ "/" + regiment.trim(), header);

				JSONObject mu = new MilitaryUnitParser().parseMembers(html);
				resp.getWriter().write(mu.toString());
			}

		} finally {
			scrapper.shutdown();
		}
	}

}
