package org.erpk.facade;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.erpk.ErepScrapper;
import org.erpk.egov4youURL;
import org.erpk.parser.Egov4YouCountryParser;
import org.json.simple.JSONArray;

public class ErpkEgovCountryServlet extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		
		String id = req.getParameter("id");

		if (id == null || id.trim().isEmpty()) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}

		ErepScrapper scrapper = new ErepScrapper();

		try {

			String html = scrapper.scrape(egov4youURL.COUNTRY_HISTORY
					.getUrl(id));
			JSONArray account = new Egov4YouCountryParser().parse(html);

			resp.getWriter().write(account.toString());

		} finally {
			scrapper.shutdown();
		}
	}

}
