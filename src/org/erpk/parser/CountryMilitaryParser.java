package org.erpk.parser;

import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class CountryMilitaryParser {

	@SuppressWarnings("unchecked")
	public JSONObject parse(String html) {

		Document doc = Jsoup.parse(html);
		JSONObject response = new JSONObject();

		response.put("as", parseAS(doc));

		return response;
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject parseAS(Document doc) {
		
		JSONObject as = new JSONObject();
		
		as.put("energy", parseEnergy(doc));
		as.put("currency", parseCurrency(doc));		
		
		return as;
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject parseEnergy(Document doc) {
		JSONObject energy = new JSONObject();
		energy.put("percentage", Integer.valueOf(doc.select("#health_units").val()));
		
		Element e = doc.select("#health_unit_cur").parents().first();
		e.select("strong").remove();
		String[] text = e.text().split("/");
		
		energy.put("current", Long.valueOf(text[0].replace(",", "").trim()));
		energy.put("required", Long.valueOf(text[1].replace(",", "").trim()));
		
		return energy;
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject parseCurrency(Document doc) {
		JSONObject energy = new JSONObject();
		energy.put("percentage", Integer.valueOf(doc.select("#currency_req").val()));
		
		Element e = doc.select("#currency_req_cur").parents().first();
		e.select("strong").remove();
		String[] text = e.text().split("/");
		
		energy.put("current", Long.valueOf(text[0].replace(",", "").trim()));
		energy.put("required", Long.valueOf(text[1].replace(",", "").trim()));
		
		return energy;
	}
}
