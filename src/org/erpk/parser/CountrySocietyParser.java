package org.erpk.parser;

import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CountrySocietyParser {

	@SuppressWarnings("unchecked")
	public JSONObject parse(String html) {

		Document doc = Jsoup.parse(html);
		JSONObject response = new JSONObject();

		response.put("citizens", parseCitizens(doc));

		return response;

	}

	@SuppressWarnings("unchecked")
	private JSONObject parseCitizens(Document doc) {
		JSONObject jsObj = new JSONObject();

		Element table = doc.select("table.citizens").first();

		Elements tableNbrs = table.select("tr>td>span.special");
		Integer activeCitizens = Integer.parseInt(tableNbrs.get(0).text()
				.replace(",", ""));
		Integer newCitizens = Integer.parseInt(tableNbrs.get(2).text()
				.replace(",", ""));

		jsObj.put("active", activeCitizens);
		jsObj.put("new", newCitizens);

		return jsObj;
	}

}
