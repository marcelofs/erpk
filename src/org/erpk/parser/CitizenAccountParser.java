package org.erpk.parser;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;

public class CitizenAccountParser {

	private static final Logger log = Logger
			.getLogger(CitizenAccountParser.class.getName());

	@SuppressWarnings("unchecked")
	public JSONObject parse(String html) {

		Document doc = Jsoup.parse(html);

		List<TextNode> citizenText = doc
				.select("div .citizen_profile_header > h2").get(0).textNodes();
		String citizenName = citizenText.get(citizenText.size() - 1).text()
				.trim();

		log.log(Level.INFO, "Name: " + citizenName);

		Element goldElement = doc
				.select(".holder > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(4)")
				.get(0);
		String gold = goldElement.text().trim();
		log.log(Level.INFO, "Gold: " + gold);

		Element ccElement = doc.select(".push_right").get(0);
		String cc = ccElement.text().trim();
		log.log(Level.INFO, "BRL: " + cc);

		JSONObject account = new JSONObject();
		account.put("eRepName", citizenName);
		account.put("currency", Double.valueOf(cc));
		account.put("gold", Double.valueOf(gold));
		return account;
	}
}