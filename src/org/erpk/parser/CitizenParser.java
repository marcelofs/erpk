package org.erpk.parser;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;

public class CitizenParser {

	private static final Logger	log	= Logger.getLogger(CitizenAccountParser.class
											.getName());

	@SuppressWarnings("unchecked")
	public JSONObject parse(String html) {

		JSONObject citizen = new JSONObject();

		String name = html.substring(html.indexOf("<title>") + 7,
				html.indexOf(" - Citizen of The New World</title>"));

		String initDescription = html.substring(html
				.indexOf("<p style=\"word-wrap:break-word\">") + 32);
		String description = initDescription.substring(0,
				initDescription.indexOf("<div class=\"clear\"></div>"));

		String initAvatar = html.substring(html
				.indexOf("style=\"background-image: url(") + 29);
		String avatar = initAvatar.substring(0, initAvatar.indexOf(");\""));

		name = Jsoup.parse(name).text();
		description = Jsoup.parse(description).text();

		citizen.put("name", StringEscapeUtils.escapeHtml4(name));
		citizen.put("description", StringEscapeUtils.escapeHtml4(description));
		citizen.put("avatar", avatar);

		log.log(Level.INFO, "User: " + citizen.toString());

		return citizen;

	}

}
