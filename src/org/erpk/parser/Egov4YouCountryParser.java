package org.erpk.parser;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Egov4YouCountryParser {

	@SuppressWarnings("unchecked")
	public JSONArray parse(String html) {

		Document doc = Jsoup.parse(html);
		Elements rows = doc.getElementsByClass("highlight");

		JSONArray array = new JSONArray();

		for (Element e : rows) {
			String day = e.child(0).text().replace(",", "");
			String citizens = e.getElementsByClass("citizens").first().text();
			String fighters = e.getElementsByClass("fighters").first().text();
			String tank = e.getElementsByClass("tank").first().text();

			JSONObject json = new JSONObject();
			json.put("day", day);
			json.put("citizens", citizens);
			json.put("fighters", fighters);
			json.put("tank", tank);

			array.add(json);
		}

		return array;
	}
}
