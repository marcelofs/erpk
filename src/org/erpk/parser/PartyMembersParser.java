package org.erpk.parser;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class PartyMembersParser {

	@SuppressWarnings("unchecked")
	public JSONArray parse(String html) {

		Document doc = Jsoup.parse(html);
		Elements members = doc.getElementsByClass("nameholder");
		JSONArray array = new JSONArray();

		for (Element e : members) {
			String name = e.attr("title");
			String id = e.attr("href").replace(
					"http://www.erepublik.com/en/citizen/profile/", "");

			JSONObject json = new JSONObject();
			json.put("name", name);
			json.put("id", id);

			array.add(json);
		}

		return array;
	}

}
