package org.erpk.parser;

import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CountryEconomyParser {

	@SuppressWarnings("unchecked")
	public JSONObject parse(String html) {

		Document doc = Jsoup.parse(html);
		JSONObject response = new JSONObject();

		response.put("bonus", parseBonus(doc));
		response.put("treasury", parseTreasury(doc));
		response.put("taxes", parseTaxes(doc));
		response.put("salary", parseWage(doc));

		return response;

	}

	@SuppressWarnings("unchecked")
	private JSONObject parseBonus(Document doc) {
		Element resourceTable = doc.getElementsByClass("resource_list").first();
		Elements rows = resourceTable.getElementsByTag("tr");

		JSONObject bonus = new JSONObject();

		int foodBonus = 0;
		int weaponBonus = 0;

		boolean grain = !"Not Available".equalsIgnoreCase(rows.get(1)
				.select("td").get(1).text());
		bonus.put("grain", grain);
		if (grain)
			foodBonus++;

		boolean fish = !"Not Available".equalsIgnoreCase(rows.get(2)
				.select("td").get(1).text());
		bonus.put("fish", fish);
		if (fish)
			foodBonus++;

		boolean fruits = !"Not Available".equalsIgnoreCase(rows.get(3)
				.select("td").get(1).text());
		bonus.put("fruits", fruits);
		if (fruits)
			foodBonus++;

		boolean cattle = !"Not Available".equalsIgnoreCase(rows.get(4)
				.select("td").get(1).text());
		bonus.put("cattle", cattle);
		if (cattle)
			foodBonus++;

		boolean deer = !"Not Available".equalsIgnoreCase(rows.get(5)
				.select("td").get(1).text());
		bonus.put("deer", deer);
		if (deer)
			foodBonus++;

		boolean iron = !"Not Available".equalsIgnoreCase(rows.get(6)
				.select("td").get(1).text());
		bonus.put("iron", iron);
		if (iron)
			weaponBonus++;

		boolean salt = !"Not Available".equalsIgnoreCase(rows.get(7)
				.select("td").get(1).text());
		bonus.put("salt", salt);
		if (salt)
			weaponBonus++;

		boolean aluminum = !"Not Available".equalsIgnoreCase(rows.get(8)
				.select("td").get(1).text());
		bonus.put("aluminum", aluminum);
		if (aluminum)
			weaponBonus++;

		boolean oil = !"Not Available".equalsIgnoreCase(rows.get(9)
				.select("td").get(1).text());
		bonus.put("oil", oil);
		if (oil)
			weaponBonus++;

		boolean rubber = !"Not Available".equalsIgnoreCase(rows.get(10)
				.select("td").get(1).text());
		bonus.put("rubber", rubber);
		if (rubber)
			weaponBonus++;

		bonus.put("weapons", weaponBonus / 5d);
		bonus.put("food", foodBonus / 5d);

		return bonus;
	}

	@SuppressWarnings("unchecked")
	private JSONObject parseTreasury(Document doc) {
		Elements treasury = doc.getElementsByClass("donation_status_table").first().child(0).children();

		JSONObject jsTreasury = new JSONObject();
		jsTreasury.put("gold", treasury.get(0).child(0).text().replace(" ", "").replace(",", ""));
		jsTreasury.put("cc", treasury.get(1).child(0).text().replace(" ", "").replace(",", ""));
		jsTreasury.put("energy", treasury.get(2).child(0).text().replace(" ", "").replace(",", ""));

		return jsTreasury;
	}

	@SuppressWarnings("unchecked")
	private JSONObject parseTaxes(Document doc) {
		JSONObject taxes = new JSONObject();

		Element table = doc.getElementsByClass("citizens").first();
		Elements rows = table.getElementsByTag("tr");

		String workTax = rows.get(1).getElementsByTag("td").get(2).text()
				.replace("%", "");
		taxes.put("workTax", Double.valueOf(workTax));

		String foodImport = rows.get(1).getElementsByTag("td").get(3).text()
				.replace("%", "");
		taxes.put("foodImport", Double.valueOf(foodImport));

		String foodVAT = rows.get(1).getElementsByTag("td").get(4).text()
				.replace("%", "");
		taxes.put("foodVAT", Double.valueOf(foodVAT));

		String weaponsImport = rows.get(2).getElementsByTag("td").get(3).text()
				.replace("%", "");
		taxes.put("weaponsImport", Double.valueOf(weaponsImport));

		String weaponsVAT = rows.get(2).getElementsByTag("td").get(4).text()
				.replace("%", "");
		taxes.put("weaponsVAT", Double.valueOf(weaponsVAT));

		String frmImport = rows.get(4).getElementsByTag("td").get(3).text()
				.replace("%", "");
		taxes.put("frmImport", Double.valueOf(frmImport));

		String wrmImport = rows.get(5).getElementsByTag("td").get(3).text()
				.replace("%", "");
		taxes.put("wrmImport", Double.valueOf(wrmImport));

		return taxes;
	}

	@SuppressWarnings("unchecked")
	private JSONObject parseWage(Document doc) {
		JSONObject wage = new JSONObject();
		Elements citizensTable = doc.getElementsByClass("citizens");
		Element tBody = citizensTable.get(1).children().first();
		String minWage = tBody.child(1).child(1).child(0).text();
		String avgWage = tBody.child(2).child(1).child(0).text();

		wage.put("minimum", minWage);
		wage.put("average", avgWage);

		return wage;
	}
}
