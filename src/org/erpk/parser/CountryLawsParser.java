package org.erpk.parser;

import org.erpk.eRepTimeCalculator;
import org.joda.time.LocalDateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CountryLawsParser {

	private final String	countryName;

	public CountryLawsParser(String countryName) {
		this.countryName = countryName;
	}

	@SuppressWarnings("unchecked")
	public JSONArray parse(String html) {

		Document doc = Jsoup.parse(html);

		Elements tableRows = doc.getElementsByClass("laws").first()
				.getElementsByTag("tr");

		JSONArray array = new JSONArray();

		for (Element row : tableRows) {
			Elements cols = row.getElementsByTag("td");

			String lawType = cols.get(0).child(0).text();
			String lawId = cols
					.get(0)
					.child(0)
					.attr("href")
					.replace(
							"http://www.erepublik.com/en/main/law/"
									+ countryName + "/", "");
			String lawDate = cols.get(1).text();
			String lawStatus = cols.get(2).text();

			JSONObject json = new JSONObject();
			json.put("type", lawType);
			json.put("id", lawId);
			json.put("date", calculateDay(lawDate));
			json.put("status", lawStatus);
			array.add(json);
		}

		return array;
	}

	private Integer calculateDay(String date) {

		Integer today = eRepTimeCalculator.geteRepDay();

		if (date.contains("seconds ago")) {
			LocalDateTime dateTime = new LocalDateTime(
					eRepTimeCalculator.eRepTimeZone);
			dateTime = dateTime.minusSeconds(Integer.valueOf(date.substring(0,
					date.indexOf(' '))));
			return eRepTimeCalculator.toErepDate(dateTime.toLocalDate());

		}

		if (date.contains("minutes ago")) {
			LocalDateTime dateTime = new LocalDateTime(
					eRepTimeCalculator.eRepTimeZone);
			dateTime = dateTime.minusMinutes(Integer.valueOf(date.substring(0,
					date.indexOf(' '))));
			return eRepTimeCalculator.toErepDate(dateTime.toLocalDate());

		}

		if (date.contains("hours ago")) {
			LocalDateTime dateTime = new LocalDateTime(
					eRepTimeCalculator.eRepTimeZone);
			dateTime = dateTime.minusHours(Integer.valueOf(date.substring(0,
					date.indexOf(' '))));
			return eRepTimeCalculator.toErepDate(dateTime.toLocalDate());
		}

		if (date.contains("yesterday"))
			return today - 1;

		if (date.contains("days ago"))
			return today - Integer.valueOf(date.replace("days ago", "").trim());

		// months ago > unknown

		return 0;
	}

	public static void main(String[] args) {
		System.out.println(new CountryLawsParser("TESTE")
				.calculateDay("16 hours ago"));
	}
}
