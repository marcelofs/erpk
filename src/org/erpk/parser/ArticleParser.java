package org.erpk.parser;

import org.json.simple.JSONObject;
import org.jsoup.Jsoup;

public class ArticleParser {

	@SuppressWarnings("unchecked")
	public JSONObject parse(String html) {

		JSONObject article = new JSONObject();

		if (html.contains("Sign In")) {
			article.put("status", "deleted");
			return article;
		}

		String initPost = html.substring(html
				.indexOf("<div class=\"post_content\">") + 26);

		String title = initPost.substring(initPost.indexOf("/1/20\">") + 7,
				initPost.indexOf("</a></h2>"));

		String initDate = initPost.substring(initPost
				.indexOf("<em class=\"date\">") + 17);
		String date = initDate.substring(0, initDate.indexOf("</em>"));

		String initAuthor = initPost.substring(initPost
				.indexOf("<em class=\"author\">by <q>") + 25);
		String author = initAuthor
				.substring(0, initAuthor.indexOf("</q></em>"));

		String initContent = initPost.substring(initPost
				.indexOf("<div class=\"full_content\">") + 26);
		String content = initContent
				.substring(0, initContent.indexOf("</div>"));

		// content = tagsPattern.matcher(content).replaceAll(" ");
		title = Jsoup.parse(title).text();
		content = Jsoup.parse(content).text();

		article.put("title", title);
		article.put("eRepDay", date);
		article.put("author", author);
		article.put("content", content);
		return article;

	}

}
