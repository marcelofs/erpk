package org.erpk.parser.laws;

import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class MPPParser implements ILawParser {

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject parse(String html) {
		Document doc = Jsoup.parse(html);

		Elements elements = doc.getElementsByClass("largepadded");

		JSONObject obj = new JSONObject();

		String proposedBy = elements.get(0).child(0).text();

		String lawText = elements.get(1).text();
		String country1 = lawText
				.substring(13, lawText.indexOf("proposed") - 1);
		String country2 = lawText.substring(lawText.indexOf("with") + 5);

		obj.put("proposedBy", proposedBy);
		obj.put("country1", country1);
		obj.put("country2", country2);

		return obj;
	}
}
