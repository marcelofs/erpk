package org.erpk.parser.laws;

public class LawParserFactory {

	public ILawParser getParser(String lawType) {

		if ("Mutual Protection Pact".equals(lawType))
			return new MPPParser();
		if ("Donate".equals(lawType))
			return new DonationParser();
//		if ("Natural Enemy".equals(lawType))
//			return new NaturalEnemyParser();
		// if ("President Impeachment".equals(lawType))
		// return new PresidentImpeachmentParser();

//		return new VoidParser();
		return null;

	}

}
