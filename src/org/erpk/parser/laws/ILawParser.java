package org.erpk.parser.laws;

import org.json.simple.JSONObject;

public interface ILawParser {

	public JSONObject parse(String html);

}
