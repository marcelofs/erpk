package org.erpk.parser.laws;

import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class NaturalEnemyParser implements ILawParser {

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject parse(String html) {
		Document doc = Jsoup.parse(html);

		Elements elements = doc.getElementsByClass("largepadded");

		JSONObject obj = new JSONObject();

		String proposedBy = elements.get(0).child(0).text();

		obj.put("proposedBy", proposedBy);
		obj.put("proposedAgainst",
				elements.get(1).text()
						.replace(" has been proposed as Natural Enemy.", ""));

		return obj;
	}
}
