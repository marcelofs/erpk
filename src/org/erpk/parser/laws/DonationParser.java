package org.erpk.parser.laws;

import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class DonationParser implements ILawParser {

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject parse(String html) {
		Document doc = Jsoup.parse(html);

		Elements elements = doc.getElementsByClass("largepadded");

		JSONObject obj = new JSONObject();

		String proposedBy = elements.get(0).child(0).text();

		String lawText = elements.get(1).text();

		String value = lawText.substring(25, lawText.indexOf("from") - 1)
				.replace(",", "");
		String destination = lawText.substring(
				lawText.indexOf("accounts to ") + 12, lawText.indexOf("?"));

		obj.put("proposedBy", proposedBy);
		obj.put("value", value);
		obj.put("destination", destination);

		return obj;
	}
}
