package org.erpk.parser.laws;

import org.json.simple.JSONObject;

public class VoidParser implements ILawParser {

	@Override
	public JSONObject parse(String html) {
		return new JSONObject();
	}
}
