package org.erpk.parser;

import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class PartyParser {

	@SuppressWarnings("unchecked")
	public JSONObject parse(String html) {

		Document doc = Jsoup.parse(html);

		JSONObject jsObj = new JSONObject();

		Elements partyMembers = doc
				.select("div.indent > div.infoholder > p.largepadded > span.special");
		jsObj.put("members", Integer.valueOf(partyMembers.first().text()));

		return jsObj;
	}

}
