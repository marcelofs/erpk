package org.erpk;

public enum egov4youURL
{
	COUNTRY_HISTORY("http://egov4you.info/country/history/{id}");

	private String	url;

	private egov4youURL(String url) {
		this.url = url;
	}

	public String getUrl(String id) {
		return this.url.replace("{id}", id);
	}
}
